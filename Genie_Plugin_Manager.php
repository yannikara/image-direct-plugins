<?php

namespace ImageDirect\Plugin\Manager;

/*
 * ----------------------------------------------------------------------------
 *
 * Plugin Name:       Image Direct Plugin Manager
 * Plugin URI:        https://www.imagedirect.com.au
 * Description:       Image Direct Plugin Manager
 * Version:           20.07.13
 * Author:            Web Team
 * Author URI:        https://www.imagedirect.com.au
 * Text Domain:       id-boilerplate
 * License:           @2020 copyright Image Direct Pty Ltd
 * License URI:
 * Domain Path:       /lang
 *
 * ----------------------------------------------------------------------------
 */



/*
 * ----------------------------------------------------------------------------
 *
 * // Prevent direct file access
 *
 * ----------------------------------------------------------------------------
 */

if (!defined('ABSPATH')) :
    exit;
endif;

/**
 * This file represents an example of the code that themes would use to register
 * the required plugins.
 *
 * It is expected that theme authors would copy and paste this code into their
 * functions.php file, and amend to suit.
 *
 * @see http://tgmpluginactivation.com/configuration/ for detailed documentation.
 *
 * @package    TGM-Plugin-Activation
 * @subpackage Example
 * @version    2.6.1 for plugin Id Boilerplate Plugins
 * @author     Thomas Griffin, Gary Jones, Juliette Reinders Folmer
 * @copyright  Copyright (c) 2011, Thomas Griffin
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       https://github.com/TGMPA/TGM-Plugin-Activation
 */
/**
 * Include the TGM_Plugin_Activation class.
 *
 * Depending on your implementation, you may want to change the include call:
 *
 * Parent Theme:
 * require_once dirname(__FILE__) . '/path/to/class-tgm-plugin-activation.php';
 *
 * Child Theme:
 * require_once get_stylesheet_directory() . '/path/to/class-tgm-plugin-activation.php';
 *
 * Plugin:
 * require_once dirname( __FILE__ ) . '/path/to/class-tgm-plugin-activation.php';
 */
require_once dirname(__FILE__) . '/includes/class-tgm-plugin-activation.php';

add_action('tgmpa_register', __NAMESPACE__ . '\register_required_plugins');

/**
 * Register the required plugins for this theme.
 *
 * In this example, we register five plugins:
 * - one included with the TGMPA library
 * - two from an external source, one from an arbitrary source, one from a GitHub repository
 * - two from the .org repo, where one demonstrates the use of the `is_callable` argument
 *
 * The variables passed to the `tgmpa()` function should be:
 * - an array of plugin arrays;
 * - optionally a configuration array.
 * If you are not changing anything in the configuration array, you can remove the array and remove the
 * variable from the function call: `tgmpa( $plugins );`.
 * In that case, the TGMPA default settings will be used.
 *
 * This function is hooked into `tgmpa_register`, which is fired on the WP `init` action on priority 10.
 */
function register_required_plugins() {
    /*
     * Array of plugin arrays. Required keys are name and slug.
     * If the source is NOT from the .org repo, then source is also required.
     */
    $plugins = array(
        /*
         * ----------------------------------------------------------------
         *
         * START HERE
         *
         * ----------------------------------------------------------------
         */


        // Smush
        array(
            'name' => '* WP Smush It',
            'slug' => 'wp-smushit',
            'required' => false,
            'force_activation' => false,
        ),
        // Duplicate Post
        array(
            'name' => '* Duplicate Post',
            'slug' => 'duplicate-post',
            'required' => false,
            'force_activation' => false,
        ),
// disable-emojis
        array(
            'name' => '* Disable Emojis',
            'slug' => 'disable-emojis',
            'required' => false,
            'force_activation' => false,
        ),
// Wordpress Importer
        array(
            'name' => '* Wordpress Importer',
            'slug' => 'wordpress-importer',
            'required' => false,
            'force_activation' => false,
        ),
// Gather Content Import
        array(
            'name' => '* Gather Content Import',
            'slug' => 'gathercontent-import',
            'required' => false,
            'force_activation' => false,
        ),
// Post Type Switcher
        array(
            'name' => '* Post Type Switcher',
            'slug' => 'post-type-switcher',
            'required' => false,
            'force_activation' => false,
        ),
// Webmaster User Role
        array(
            'name' => '* Webmaster User Role',
            'slug' => 'webmaster-user-role',
            'required' => false,
            'force_activation' => false,
        ),
// User Role Editor
        array(
            'name' => '* User Role Editor',
            'slug' => 'user-role-editor',
            'required' => false,
            'force_activation' => false,
        ),
//BJ Lazy Load
//        array(
//            'name' => '* Unveil Lazy Load',
//            'slug' => 'unveil-lazy-load',
//            'required' => false,
//            'force_activation' => false,
//        ),
// Wordpress PB SEO Images
// 2020-02-28 by Thomasc - Remove because of causing issue with ACF field adding content to empty field.
//                array(
//                        'name' => '* PB SEO Friendly Images',
//                        'slug' => 'pb-seo-friendly-images',
//                        'required' => false,
//                        'force_activation' => false,
//                ),
// YouTube Responsive Widget
        array(
            'name' => '* YouTube Responsive Widget',
            'slug' => 'youtube-widget-responsive',
            'required' => false,
            'force_activation' => false,
        ),
// Force Regenerate Thumbnails
        array(
            'name' => '* Force Regenerate Thumbnails',
            'slug' => 'force-regenerate-thumbnails',
            'required' => false,
            'force_activation' => false,
        ),
// Download Monitor
        array(
            'name' => '* Downloads Monitor',
            'slug' => 'download-monitor',
            'required' => false,
            'force_activation' => false,
        ),
// White Label CMS
        array(
            'name' => '* White Label CMS',
            'slug' => 'white-label-cms',
            'required' => false,
            'force_activation' => false,
        ),
// Google Analytics Dashboard for WP
//                array(
//                        'name' => '* Google Analytics Dashboard for WP',
//                        'slug' => 'google-analytics-dashboard-for-wp',
//                        'required' => false,
//                        'force_activation' => false,
//                ),
        array(
            'name' => '* GAinWP Google Analytics Integration',
            'slug' => 'ga-in',
            'required' => false,
            'force_activation' => false,
        ),
// Invisible reCaptcha for WordPress
        array(
            'name' => '* Invisible reCaptcha for WordPress',
            'slug' => 'invisible-recaptcha',
            'required' => false,
            'force_activation' => false,
        ),
// SVG
        array(
            'name' => '@ SVG Support',
            'slug' => 'svg-support',
            'required' => false,
            'force_activation' => false,
        ),
// SVG
        array(
            'name' => '@ Safe SVG',
            'slug' => 'safe-svg',
            'required' => false,
            'force_activation' => false,
        ),
// Enable media Replace
        array(
            'name' => '@ Enable media Replace',
            'slug' => 'enable-media-replace',
            'required' => false,
            'force_activation' => false,
        ),
// Plainview Activity Monitor
        array(
            'name' => '@ Plainview Activity Monitor',
            'slug' => 'plainview-activity-monitor',
            'required' => true,
            'force_activation' => true,
        ),
//Jetpack
        array(
            'name' => '@ Jetpack by WordPress',
            'slug' => 'jetpack',
            'required' => true,
            'force_activation' => true,
        ),
// Wordpress SEO by Yoast
        array(
            'name' => '@ WordPress SEO by Yoast',
            'slug' => 'wordpress-seo',
            'required' => true,
            'force_activation' => true,
        ),
// Advanced Custom Field Pro ACF5
        array(
            'name' => '® Advanced Custom Fields Add-On: ACF Pro',
            'slug' => 'advanced-custom-fields-pro',
            'source' => dirname(__FILE__) . '/assets/plugins/' . 'advanced-custom-fields-pro.zip',
            'required' => true,
            'force_activation' => true
        ),
        // Advanced Custom Field OpenStreetMap ACF5
        array(
            'name' => '® Advanced Custom Fields Add-On: OpenStreetMap',
            'slug' => 'acf-openstreetmap-field',
            'required' => true,
            'force_activation' => true
        ),
// Advanced Custom Fields Add-On: Font Awesome
        array(
            'name' => '@ Advanced Custom Fields Add-On: Font Awesome',
            'slug' => 'advanced-custom-fields-font-awesome',
            'required' => true,
            'force_activation' => true,
        ),
// AH Display Widgets
        array(
            'name' => '@ ACF Duplicate Repeater', // The plugin name
            'slug' => 'acf-duplicate-repeater', // The plugin slug (typically the folder name)
            'source' => dirname(__FILE__) . '/assets/plugins/acf-duplicate-repeater.zip',
            'required' => false,
            'force_activation' => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
        ),
// Advanced Custom Fields Add-On: Contact Form 7
//        array(
//            'name' => '@ Advanced Custom Fields Add-On: Contact Form 7',
//            'slug' => 'advanced-custom-fields-contact-form-7-field',
//            'required' => true,
//            'force_activation' => true,
//        ),
// Contact Form 7 - contact form generator
        array(
            'name' => '@ Contact Form 7',
            'slug' => 'contact-form-7',
            'required' => true,
            'force_activation' => true,
        ),
// Contact Form DB - Contact form 7 database extension
        array(
            'name' => '@ Contact Form DB',
            'slug' => 'contact-form-7-to-database-extension',
            'required' => false,
            'force_activation' => false,
        ),
        array(
            'name' => '@ Contact Form CFDB7',
            'slug' => 'contact-form-cfdb7',
            'required' => false,
            'force_activation' => false,
        ),
// Video User Manuals
        array(
            'name' => '® Video User Manuals', // The plugin name
            'slug' => 'video-user-manuals', // The plugin slug (typically the folder name)
            'source' => dirname(__FILE__) . '/assets/plugins/' . 'video-user-manuals-2.3.2.zip', // The plugin source
            'required' => true,
            'force_activation' => true, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation' => true, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
        ),
// Editor Full Width Gutenberg
        array(
            'name' => 'Editor Full Width Gutenberg', // The plugin name
            'slug' => 'editor-full-width', // The plugin slug (typically the folder name)
            'required' => true,
            'force_activation' => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
        ),
// AH Display Widgets
        array(
            'name' => '* AH Display Widgets', // The plugin name
            'slug' => 'ah-display-widgets', // The plugin slug (typically the folder name)
            'required' => false,
            'force_activation' => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
//			'external_url' => 'https://github.com/HechtMediaArts/ah-display-widgets', // If set, overrides default API URL and points to an external URL.
        ),
// Optimize Database after Deleting Revisions
        array(
            'name' => '* Optimize Database after Deleting Revisions', // The plugin name
            'slug' => 'rvg-optimize-database', // The plugin slug (typically the folder name)
            'required' => false,
            'force_activation' => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
        ),
// Redirection
        array(
            'name' => '* Redirection', // The plugin name
            'slug' => 'redirection', // The plugin slug (typically the folder name)
            'required' => false,
            'force_activation' => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
        ),
// Facebook Pixel
        array(
            'name' => 'Official Facebook Pixel', // The plugin name
            'slug' => 'official-facebook-pixel', // The plugin slug (typically the folder name)
            'required' => false,
            'force_activation' => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
        ),
// Facebook for Woocommerce
        array(
            'name' => 'Facebook for Woocommerce', // The plugin name
            'slug' => 'facebook-for-woocommerce', // The plugin slug (typically the folder name)
            'required' => false,
            'force_activation' => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
        ),
            /*
             * ----------------------------------------------------------------
             *
             * STOP HERE
             *
             * ----------------------------------------------------------------
             */
    );
    /*
     * Array of configuration settings. Amend each line as needed.
     *
     * TGMPA will start providing localized text strings soon. If you already have translations of our standard
     * strings available, please help us make TGMPA even better by giving us access to these translations or by
     * sending in a pull-request with .po file(s) with the translations.
     *
     * Only uncomment the strings in the config array if you want to customize the strings.
     */
    $config = array(
        'id' => 'id-boilerplate', // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => '', // Default absolute path to bundled plugins.
        'menu' => 'tgmpa-install-plugins', // Menu slug.
        'parent_slug' => 'themes.php', // Parent menu slug.
        'capability' => 'manage_options', // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
        'has_notices' => true, // Show admin notices or not.
        'dismissable' => true, // If false, a user cannot dismiss the nag message.
        'dismiss_msg' => '', // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false, // Automatically activate plugins after installation or not.
        'message' => '', // Message to output right before the plugins table.

            /*
              'strings'      => array(
              'page_title'                      => __( 'Install Required Plugins', 'id-boilerplate' ),
              'menu_title'                      => __( 'Install Plugins', 'id-boilerplate' ),
              /* translators: %s: plugin name. * /
              'installing'                      => __( 'Installing Plugin: %s', 'id-boilerplate' ),
              /* translators: %s: plugin name. * /
              'updating'                        => __( 'Updating Plugin: %s', 'id-boilerplate' ),
              'oops'                            => __( 'Something went wrong with the plugin API.', 'id-boilerplate' ),
              'notice_can_install_required'     => _n_noop(
              /* translators: 1: plugin name(s). * /
              'This theme requires the following plugin: %1$s.',
              'This theme requires the following plugins: %1$s.',
              'id-boilerplate'
              ),
              'notice_can_install_recommended'  => _n_noop(
              /* translators: 1: plugin name(s). * /
              'This theme recommends the following plugin: %1$s.',
              'This theme recommends the following plugins: %1$s.',
              'id-boilerplate'
              ),
              'notice_ask_to_update'            => _n_noop(
              /* translators: 1: plugin name(s). * /
              'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.',
              'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.',
              'id-boilerplate'
              ),
              'notice_ask_to_update_maybe'      => _n_noop(
              /* translators: 1: plugin name(s). * /
              'There is an update available for: %1$s.',
              'There are updates available for the following plugins: %1$s.',
              'id-boilerplate'
              ),
              'notice_can_activate_required'    => _n_noop(
              /* translators: 1: plugin name(s). * /
              'The following required plugin is currently inactive: %1$s.',
              'The following required plugins are currently inactive: %1$s.',
              'id-boilerplate'
              ),
              'notice_can_activate_recommended' => _n_noop(
              /* translators: 1: plugin name(s). * /
              'The following recommended plugin is currently inactive: %1$s.',
              'The following recommended plugins are currently inactive: %1$s.',
              'id-boilerplate'
              ),
              'install_link'                    => _n_noop(
              'Begin installing plugin',
              'Begin installing plugins',
              'id-boilerplate'
              ),
              'update_link' 					  => _n_noop(
              'Begin updating plugin',
              'Begin updating plugins',
              'id-boilerplate'
              ),
              'activate_link'                   => _n_noop(
              'Begin activating plugin',
              'Begin activating plugins',
              'id-boilerplate'
              ),
              'return'                          => __( 'Return to Required Plugins Installer', 'id-boilerplate' ),
              'plugin_activated'                => __( 'Plugin activated successfully.', 'id-boilerplate' ),
              'activated_successfully'          => __( 'The following plugin was activated successfully:', 'id-boilerplate' ),
              /* translators: 1: plugin name. * /
              'plugin_already_active'           => __( 'No action taken. Plugin %1$s was already active.', 'id-boilerplate' ),
              /* translators: 1: plugin name. * /
              'plugin_needs_higher_version'     => __( 'Plugin not activated. A higher version of %s is needed for this theme. Please update the plugin.', 'id-boilerplate' ),
              /* translators: 1: dashboard link. * /
              'complete'                        => __( 'All plugins installed and activated successfully. %1$s', 'id-boilerplate' ),
              'dismiss'                         => __( 'Dismiss this notice', 'id-boilerplate' ),
              'notice_cannot_install_activate'  => __( 'There are one or more required or recommended plugins to install, update or activate.', 'id-boilerplate' ),
              'contact_admin'                   => __( 'Please contact the administrator of this site for help.', 'id-boilerplate' ),

              'nag_type'                        => '', // Determines admin notice type - can only be one of the typical WP notice classes, such as 'updated', 'update-nag', 'notice-warning', 'notice-info' or 'error'. Some of which may not work as expected in older WP versions.
              ),
             */
    );

    tgmpa($plugins, $config);
}
